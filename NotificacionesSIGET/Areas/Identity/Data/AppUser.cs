﻿using Microsoft.AspNetCore.Identity;
using NotificacionesSIGET.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Areas.Identity.Data
{
    public class AppUser : IdentityUser
    {
        [MaxLength(280)]
        [Required]
        public string Nombres { get; set; }

        [MaxLength(280)]
        [Required]
        public string Apellidos { get; set; }

        [Required]
        public bool EsEmpleado { get; set; }

        public bool FirstPassword { get; set; }

        public bool Activo { get; set; }

        public string nombreCompleto()
        {
            return $" {Nombres} {Apellidos} ";
        }

        public virtual TipoPersona TipoPersona { get; set; }
        public int TipoPersonaId { get; set; }
        public virtual Empresa empresa { get; set; }
        [Display(Name = "Empresa")]
        public int EmpresaId { get; set; }
    }
}
