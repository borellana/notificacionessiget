﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class tipoDocumento2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "FechaDescarga",
                table: "notificacion",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Representante",
                table: "empresa",
                nullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "Fecha",
                table: "documentos",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<string>(
                name: "Numero",
                table: "documentos",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "TipoDocumentoId",
                table: "documentos",
                nullable: true,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "TipoPersonaId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateTable(
                name: "TipoDocumentos",
                columns: table => new
                {
                    id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoDocumentos", x => x.id);
                });

            migrationBuilder.CreateTable(
                name: "TipoPersonas",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Nombre = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TipoPersonas", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_documentos_TipoDocumentoId",
                table: "documentos",
                column: "TipoDocumentoId");

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_TipoPersonaId",
                table: "AspNetUsers",
                column: "TipoPersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaId",
                table: "AspNetUsers",
                column: "TipoPersonaId",
                principalTable: "TipoPersonas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_documentos_TipoDocumentos_TipoDocumentoId",
                table: "documentos",
                column: "TipoDocumentoId",
                principalTable: "TipoDocumentos",
                principalColumn: "id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_documentos_TipoDocumentos_TipoDocumentoId",
                table: "documentos");

            migrationBuilder.DropTable(
                name: "TipoDocumentos");

            migrationBuilder.DropTable(
                name: "TipoPersonas");

            migrationBuilder.DropIndex(
                name: "IX_documentos_TipoDocumentoId",
                table: "documentos");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_TipoPersonaId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "FechaDescarga",
                table: "notificacion");

            migrationBuilder.DropColumn(
                name: "Representante",
                table: "empresa");

            migrationBuilder.DropColumn(
                name: "Fecha",
                table: "documentos");

            migrationBuilder.DropColumn(
                name: "Numero",
                table: "documentos");

            migrationBuilder.DropColumn(
                name: "TipoDocumentoId",
                table: "documentos");

            migrationBuilder.DropColumn(
                name: "TipoPersonaId",
                table: "AspNetUsers");
        }
    }
}
