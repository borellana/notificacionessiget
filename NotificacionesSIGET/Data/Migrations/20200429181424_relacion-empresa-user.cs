﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class relacionempresauser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "usuarioId",
                table: "empresa",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_empresa_usuarioId",
                table: "empresa",
                column: "usuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_empresa_AspNetUsers_usuarioId",
                table: "empresa",
                column: "usuarioId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_empresa_AspNetUsers_usuarioId",
                table: "empresa");

            migrationBuilder.DropIndex(
                name: "IX_empresa_usuarioId",
                table: "empresa");

            migrationBuilder.DropColumn(
                name: "usuarioId",
                table: "empresa");
        }
    }
}
