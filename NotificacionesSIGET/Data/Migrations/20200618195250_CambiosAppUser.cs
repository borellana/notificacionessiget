﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class CambiosAppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaid",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "TipoPersonaid",
                table: "AspNetUsers",
                newName: "TipoPersonaId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_TipoPersonaid",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_TipoPersonaId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaId",
                table: "AspNetUsers",
                column: "TipoPersonaId",
                principalTable: "TipoPersonas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "TipoPersonaId",
                table: "AspNetUsers",
                newName: "TipoPersonaid");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_TipoPersonaId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_TipoPersonaid");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaid",
                table: "AspNetUsers",
                column: "TipoPersonaid",
                principalTable: "TipoPersonas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
