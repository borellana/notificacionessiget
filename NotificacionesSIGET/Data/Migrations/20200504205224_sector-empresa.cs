﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class sectorempresa : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_sector_empresa_EmpresaId",
                table: "sector");

            migrationBuilder.DropIndex(
                name: "IX_sector_EmpresaId",
                table: "sector");

            migrationBuilder.DropColumn(
                name: "EmpresaId",
                table: "sector");

            migrationBuilder.AddColumn<int>(
                name: "SectorId",
                table: "empresa",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_empresa_SectorId",
                table: "empresa",
                column: "SectorId");

            migrationBuilder.AddForeignKey(
                name: "FK_empresa_sector_SectorId",
                table: "empresa",
                column: "SectorId",
                principalTable: "sector",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_empresa_sector_SectorId",
                table: "empresa");

            migrationBuilder.DropIndex(
                name: "IX_empresa_SectorId",
                table: "empresa");

            migrationBuilder.DropColumn(
                name: "SectorId",
                table: "empresa");

            migrationBuilder.AddColumn<int>(
                name: "EmpresaId",
                table: "sector",
                type: "int",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_sector_EmpresaId",
                table: "sector",
                column: "EmpresaId");

            migrationBuilder.AddForeignKey(
                name: "FK_sector_empresa_EmpresaId",
                table: "sector",
                column: "EmpresaId",
                principalTable: "empresa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
