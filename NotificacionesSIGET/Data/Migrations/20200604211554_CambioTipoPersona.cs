﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class CambioTipoPersona : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_empresa_empresaId",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "empresaId",
                table: "AspNetUsers",
                newName: "EmpresaId");

            migrationBuilder.RenameColumn(
                name: "TipoPersonaId",
                table: "AspNetUsers",
                newName: "TipoPersonaid");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_empresaId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_EmpresaId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_TipoPersonaId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_TipoPersonaid");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_empresa_EmpresaId",
                table: "AspNetUsers",
                column: "EmpresaId",
                principalTable: "empresa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaid",
                table: "AspNetUsers",
                column: "TipoPersonaid",
                principalTable: "TipoPersonas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_empresa_EmpresaId",
                table: "AspNetUsers");

            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaid",
                table: "AspNetUsers");

            migrationBuilder.RenameColumn(
                name: "TipoPersonaid",
                table: "AspNetUsers",
                newName: "TipoPersonaId");

            migrationBuilder.RenameColumn(
                name: "EmpresaId",
                table: "AspNetUsers",
                newName: "empresaId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_TipoPersonaid",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_TipoPersonaId");

            migrationBuilder.RenameIndex(
                name: "IX_AspNetUsers_EmpresaId",
                table: "AspNetUsers",
                newName: "IX_AspNetUsers_empresaId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_TipoPersonas_TipoPersonaId",
                table: "AspNetUsers",
                column: "TipoPersonaId",
                principalTable: "TipoPersonas",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_empresa_empresaId",
                table: "AspNetUsers",
                column: "empresaId",
                principalTable: "empresa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
