﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class AppUser : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_empresa_AspNetUsers_usuarioId",
                table: "empresa");

            migrationBuilder.DropIndex(
                name: "IX_empresa_usuarioId",
                table: "empresa");

            migrationBuilder.DropColumn(
                name: "usuarioId",
                table: "empresa");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "AspNetUsers",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<bool>(
                name: "Activo",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Apellidos",
                table: "AspNetUsers",
                maxLength: 280,
                nullable: true);

            migrationBuilder.AddColumn<bool>(
                name: "EsEmpleado",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Nombres",
                table: "AspNetUsers",
                maxLength: 280,
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "empresaId",
                table: "AspNetUsers",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_AspNetUsers_empresaId",
                table: "AspNetUsers",
                column: "empresaId");

            migrationBuilder.AddForeignKey(
                name: "FK_AspNetUsers_empresa_empresaId",
                table: "AspNetUsers",
                column: "empresaId",
                principalTable: "empresa",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_AspNetUsers_empresa_empresaId",
                table: "AspNetUsers");

            migrationBuilder.DropIndex(
                name: "IX_AspNetUsers_empresaId",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Activo",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Apellidos",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "EsEmpleado",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "Nombres",
                table: "AspNetUsers");

            migrationBuilder.DropColumn(
                name: "empresaId",
                table: "AspNetUsers");

            migrationBuilder.AddColumn<string>(
                name: "usuarioId",
                table: "empresa",
                type: "nvarchar(450)",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_empresa_usuarioId",
                table: "empresa",
                column: "usuarioId");

            migrationBuilder.AddForeignKey(
                name: "FK_empresa_AspNetUsers_usuarioId",
                table: "empresa",
                column: "usuarioId",
                principalTable: "AspNetUsers",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
