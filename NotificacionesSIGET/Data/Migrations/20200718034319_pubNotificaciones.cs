﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace NotificacionesSIGET.Data.Migrations
{
    public partial class pubNotificaciones : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PublicadoPor",
                table: "notificacion",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PublicadoPor",
                table: "notificacion");
        }
    }
}
