﻿using Microsoft.AspNetCore.Identity;
using NotificacionesSIGET.Areas.Identity.Data;
using NotificacionesSIGET.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Data
{
    public class IniciarData
    {
        public static void IniciarBase(ApplicationDbContext context,
                                     UserManager<AppUser> userManager,
                                     RoleManager<IdentityRole> roleManager) 
        {
            if (context.Users.Any())
            {
                return;
            }

            var clave = "$iget2020.";

            var sector = new Sector[] {
                new Sector { Nombre = "Telecomunicaciones"},
                new Sector { Nombre = "Electricidad"}
            };
            context.AddRange(sector);

            var userAdministrador = new AppUser
            {
                
                Email = "administrador@siget.gob.sv",
                UserName = "administrador@siget.gob.sv",
                Nombres = "Administrador",
                Apellidos = "SIGET",
                EsEmpleado = true,
            };
            userManager.CreateAsync(userAdministrador,  clave).Wait();
            var rolAdministrador = new IdentityRole { Name = "Administrador" };
            roleManager.CreateAsync(rolAdministrador).Wait();
            userManager.AddToRoleAsync(userAdministrador, rolAdministrador.Name).Wait();

            var rol = new IdentityRole { Name = "Notificador" };
            roleManager.CreateAsync(rol).Wait();
            rol = new IdentityRole { Name = "Usuario" };
            roleManager.CreateAsync(rol).Wait();

            var tipoDocumento = new TipoDocumento[] {
                new TipoDocumento { nombre = "Acuerdo" },
                new TipoDocumento { nombre = "Resolucion" },
                new TipoDocumento { nombre = "Carta" }
            };
            context.AddRange(tipoDocumento);

            var tipoPersona = new TipoPersona[] {
                new TipoPersona { nombre = "Natural"},
                new TipoPersona { nombre = "Juridica"},
                new TipoPersona { nombre = "Gobierno"}
            };
            context.AddRange(tipoPersona);

            var unidades = new Unidad[] {
                new Unidad { Nombre = "Legal"},
                new Unidad { Nombre = "Junta de Directores"}
            };
            context.AddRange(unidades);

            var sectorTelecomunicaciones = int.Parse(context.sector.Where(x => x.Nombre == "Telecomunicaciones").Select(x => x.Id).ToString());
            var sectorElectricidad = int.Parse(context.sector.Where(x => x.Nombre == "Electricidad").Select(x => x.Id).ToString());

            var empresa = new Empresa[] {
                new Empresa{ Nombre ="Claro", SectorId=sectorTelecomunicaciones, Representante=""},
                new Empresa{ Nombre ="Movistar", SectorId=sectorTelecomunicaciones, Representante=""},
                new Empresa{ Nombre ="Tigo", SectorId=sectorTelecomunicaciones, Representante=""},
                new Empresa{ Nombre ="Digicel", SectorId=sectorTelecomunicaciones, Representante=""},
                new Empresa{ Nombre ="DelSur", SectorId=sectorElectricidad, Representante=""},
                new Empresa{ Nombre ="AES", SectorId=sectorElectricidad, Representante=""}

            };
            context.AddRange(empresa);
        }
    }
}
