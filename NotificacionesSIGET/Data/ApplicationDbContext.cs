﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Models;
using NotificacionesSIGET.ViewModels;

namespace NotificacionesSIGET.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        public DbSet<Parametros> parametros { get; set; }
        public DbSet<LogNotificaciones> log { get; set; }
        public DbSet<Documento> documentos { get; set; }
        public DbSet<Empresa> empresa { get; set; }
        public DbSet<Unidad> unidad { get; set; }
        public DbSet<Notificacion> notificacion { get; set; }
        public DbSet<Sector> sector { get; set; }
        public DbSet<TipoPersona> TipoPersonas { get; set; }
        public DbSet<TipoDocumento> TipoDocumentos { get; set; }
        public DbSet<Grupo> Grupo { get; set; }
        public DbSet<GrupoEmpresa> GrupoEmpresa { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<GrupoEmpresa>(grupoEmpresa =>
            {
                grupoEmpresa.HasNoKey();
            });
        }

    }
}
