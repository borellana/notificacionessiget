﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Areas.Identity.Data;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using NotificacionesSIGET.Tools;
using NotificacionesSIGET.ViewModels;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Notificador")]
    public class DocumentosController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly IHostingEnvironment _hostingEnvironment;
        private readonly UserManager<AppUser> _userManager;


        public DocumentosController(ApplicationDbContext context,
                                    IHostingEnvironment hostingEnvironment, 
                                    UserManager<AppUser> userManager)
        {
            _context = context;
            _hostingEnvironment = hostingEnvironment;
            _userManager = userManager;

        }

        // GET: Documentoes
        public async Task<IActionResult> Index()
        {
            try
            {
                Herramientas.EscribirLog("Documento-Index", "Ingreso a index de documentos", "",
                                         User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var documentos = _context.documentos;

                var result = (from Documento in documentos
                              join u in _userManager.Users on Documento.PublicadoPor equals u.Id
                              select new Documento
                              {
                                  id = Documento.id,
                                  Nombre = Documento.Nombre,
                                  descripcion = Documento.descripcion,
                                  Numero = Documento.Numero,
                                  Fecha = Documento.Fecha,
                                  PublicadoPor =  u.Nombres + " " + u.Apellidos
                }).ToListAsync();


                return View(await result);

            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Documentoes/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Documento-Details", "Ingreso a Details de documentos", id.ToString(),
                                         User.FindFirst(ClaimTypes.NameIdentifier).Value, _context); ;

                if (id == null)
                {
                    return NotFound();
                }

                var documento = await _context.documentos
                    .FirstOrDefaultAsync(m => m.id == id);
                if (documento == null)
                {
                    return NotFound();
                }

                return View(documento);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Documentoes/Create
        public IActionResult Create()
        {
            ViewData["TipoDocumentoId"] = new SelectList(_context.TipoDocumentos, "id", "nombre");
            return View();
        }

        // POST: Documentoes/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("id,Nombre,descripcion,FFArchivo,Numero,Fecha,TipoDocumentoId")] Documento documento)
        {
            if (ModelState.IsValid)
            {
                try
                {
                    Herramientas.EscribirLog("Documento-Create", "Ingreso a Create de documentos", documento.Nombre,
                                                User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                    if (documento.FFArchivo != null)
                    {
                        string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "Documentos");
                        string filePath = Path.Combine(uploadsFolder, documento.FFArchivo.FileName);
                        documento.FFArchivo.CopyTo(new FileStream(filePath, FileMode.Create));
                    }

                    documento.Nombre = (documento.FFArchivo.FileName.ToString());
                    documento.PublicadoPor = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                    _context.Add(documento);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                catch (Exception ex)
                {
                    Herramientas.EscribirLog(ex, _context);
                    return View(documento);
                }
            }
            return View(documento);
        }

        // GET: Documentoes/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Documento-Edit", "Ingreso a Edit de documentos", id.ToString(),
                                         User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var documento = await _context.documentos.FindAsync(id);
                if (documento == null)
                {
                    return NotFound();
                }
                ViewData["TipoDocumentoId"] = new SelectList(_context.TipoDocumentos, "id", "nombre");
                return View(documento);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Documentoes/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("id,Nombre,descripcion,Numero,Fecha,TipoDocumentoId")] Documento documento)
        {
            try
            {
                Herramientas.EscribirLog("Documento-Edit", "Se edito de documentos", id.ToString(),
                    User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id != documento.id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(documento);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!DocumentoExists(documento.id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(documento);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Documentoes/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Documento-Delete", "Se ingresa a Delete del documentos", id.ToString(),
                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var documento = await _context.documentos
                    .FirstOrDefaultAsync(m => m.id == id);
                if (documento == null)
                {
                    return NotFound();
                }

                return View(documento);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Documentoes/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Herramientas.EscribirLog("Documento-DeleteConfirmed", "Se ingresa a DeleteConfirmed del documentos",
                                         "Id de documento: " + id, User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var notificaciones = _context.notificacion.Where(x => x.DocumentoId == id).Count();
                if (notificaciones != 0)
                {
                    ViewData["Mensaje"] = "El documento no puede ser eliminador, existen notificaciones relacionadas con este documento.";
                    var detalle = await _context.documentos.FirstOrDefaultAsync(m => m.id == id);
                    return View(detalle);
                }

                var nombreArchivo = _context.documentos.Where(x => x.id == id).Select(x => x.Nombre).FirstOrDefault();

                string uploadsFolder = Path.Combine(_hostingEnvironment.WebRootPath, "Documentos");
                string filePath = Path.Combine(uploadsFolder, nombreArchivo);

                System.IO.File.Delete(filePath);

                var documento = await _context.documentos.FindAsync(id);
                _context.documentos.Remove(documento);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        private bool DocumentoExists(int id)
        {
            return _context.documentos.Any(e => e.id == id);
        }
    }
}
