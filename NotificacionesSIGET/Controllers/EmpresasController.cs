﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using Microsoft.AspNetCore.Authorization;
using NotificacionesSIGET.Tools;
using System.Security.Claims;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Administrador, Notificador")]
    public class EmpresasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public EmpresasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Empresas
        public async Task<IActionResult> Index()
        {
            try
            {

                Herramientas.EscribirLog("Empresas-Index", "Se ingresa a Index del Empresas", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                
                    var applicationDbContext = _context.empresa.Include(e => e.sector);
                    return View(await applicationDbContext.ToListAsync());
                
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex,_context);
                return View();
            }
        }

        // GET: Empresas/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {

                Herramientas.EscribirLog("Empresas-Details", "Se ingresa a Details del Empresas", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
            {
                return NotFound();
            }

            var empresa = await _context.empresa
                .Include(e => e.sector)
                .FirstOrDefaultAsync(m => m.Id == id);
            if (empresa == null)
            {
                return NotFound();
            }

            return View(empresa);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Empresas/Create
        public IActionResult Create()
        {
            try
            {

                Herramientas.EscribirLog("Documento-Create", "Se ingresa a Create del Empresas", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                ViewData["SectorId"] = new SelectList(_context.sector, "Id", "Nombre");
                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Empresas/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,SectorId,Representante")] Empresa empresa)
        {
            try
            {
                Herramientas.EscribirLog("Empresas-Create", "Se Crea la Empresa", empresa.Id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (ModelState.IsValid)
                {
                    _context.Add(empresa);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                ViewData["SectorId"] = new SelectList(_context.sector, "Id", "sector", empresa.sector);
                return View(empresa);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Empresas/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {

                Herramientas.EscribirLog("Documento-Delete", "Se ingresa a Delete del Empresas", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var empresa = await _context.empresa.FindAsync(id);
                if (empresa == null)
                {
                    return NotFound();
                }
                ViewData["SectorId"] = new SelectList(_context.sector, "Id", "Nombre", empresa.sector);
                return View(empresa);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Empresas/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,SectorId,Representante")] Empresa empresa)
        {
            try
            {

                Herramientas.EscribirLog("Documento-Edit", "Se ingresa a Edit del Empresas", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id != empresa.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(empresa);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!EmpresaExists(empresa.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                ViewData["SectorId"] = new SelectList(_context.sector, "Id", "sector", empresa.sector);
                return View(empresa);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Empresas/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {

                Herramientas.EscribirLog("Documento-Delete", "Se ingresa a Delete del Empresas", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var empresa = await _context.empresa
                    .Include(e => e.sector)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (empresa == null)
                {
                    return NotFound();
                }

                return View(empresa);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Empresas/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {

                Herramientas.EscribirLog("Documento-DeleteConfirmed", "Se DeleteConfirmed del Empresas", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                var empresa = await _context.empresa.FindAsync(id);
                _context.empresa.Remove(empresa);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }

        }

        private bool EmpresaExists(int id)
        {
            return _context.empresa.Any(e => e.Id == id);
        }
    }
}
