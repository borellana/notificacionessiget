﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NotificacionesSIGET.ViewModels;
using Microsoft.AspNetCore.Authorization;
using NotificacionesSIGET.Tools;
using System.Security.Claims;
using NotificacionesSIGET.Data;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class RolesController : Controller
    {
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly ApplicationDbContext _context;


        public RolesController(RoleManager<IdentityRole> roleManager,
                                ApplicationDbContext context)
        {
            _roleManager = roleManager;
            _context = context;
        }

        [HttpGet]
        public IActionResult Index()
        {
            try
            {

                Herramientas.EscribirLog("Roles-Index", "Se ingresa a Index del Roles", "",
                                                    User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                return View(_roleManager.Roles);


            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        public IActionResult Create()
        {
            try
            {

                Herramientas.EscribirLog("Roles-Create", "Se ingresa a Create del Roles", "",
                                                    User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Create(string RolName)
        {
            try
            {

                Herramientas.EscribirLog("Roles-Create", "Se crea Rol", RolName,
                                                    User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (!string.IsNullOrEmpty(RolName))
            {
                IdentityResult result = await _roleManager.CreateAsync(new IdentityRole(RolName));
                if (result.Succeeded)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }
            }
            return View(RolName);
        }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
    }
}

        public ActionResult Details(string id)
        {
            try
            {

                Herramientas.EscribirLog("Roles-Details", "Se ingresa a Details del Roles", id,
                                                    User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                return View(_roleManager.Roles.FirstOrDefault(x => x.Id == id));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

    }
}