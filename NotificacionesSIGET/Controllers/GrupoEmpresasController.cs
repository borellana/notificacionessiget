﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;

namespace NotificacionesSIGET.Controllers
{
    [Authorize]
    public class GrupoEmpresasController : Controller
    {
        private readonly ApplicationDbContext _context;

        public GrupoEmpresasController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Grupos
        public async Task<IActionResult> Index()
        {
            var Grupos = _context.GrupoEmpresa;

            var result = from grupoEmpresa in Grupos
                         select new GrupoEmpresa
                         {
                             Grupo = grupoEmpresa.Grupo
                               // todo: pendiente enviar la cuenta los casos por grupo
                             //Empresa = grupoEmpresa.Empresa
                         };

            return View(await result.ToListAsync());
        }

        // GET: Grupos/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grupoEmpresa = _context.GrupoEmpresa.Include(x=>x.Grupo).Include(x=> x.Empresa)
                .Where(m => m.Grupo.Id == id).ToList();
                

            if (grupoEmpresa == null)
            {
                return NotFound();
            }

            return View(grupoEmpresa);
        }

        // GET: Grupos/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var grupo = await _context.Grupo.FindAsync(id);
            if (grupo == null)
            {
                return NotFound();
            }
            return View(grupo);
        }
    }
}
