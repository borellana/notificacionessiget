﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.ViewModels;

namespace NotificacionesSIGET.Controllers
{
    public class ReportesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ReportesController(ApplicationDbContext context)
        {
            _context = context;
        }

        public async Task<IActionResult> Index()
        {
            var notificaciones = _context.notificacion
                                    .Include(x => x.unidad)
                                    .Include(x => x.empresa)
                                    .Include(x => x.documento);

            var result =  (from vmReporte in notificaciones
                          select new VMReporte
                          {
                              Id = vmReporte.Id,
                              Comentario = vmReporte.Comentario,
                              FechaCarga = vmReporte.Fecha,
                              fechaDescarga = vmReporte.FechaDescarga,
                              NombreDocumento = vmReporte.documento.Nombre,
                              DescripcionDocumento = vmReporte.documento.descripcion,
                              NumeroDocumento = vmReporte.documento.Numero,
                              UnidadNombre = vmReporte.unidad.Nombre,
                              EmpresaNombre = vmReporte.empresa.Nombre
                          });               

            return View(await result.ToListAsync());
        }

        //public async Task<IActionResult> ExportToCSV(VMReporte Reporte)
        //{
            //var builder = new StringBuilder();
            //builder.AppendLine("Columnas");
            //foreach (var algo in Reporte) {
            //    builder.AppendLine($"{algo.},{algo.}");
            //}   

        //    return File(Encoding.UTF8.GetBytes(builder.ToString()), "text/csv", "NotificacionesInfo.csv");

        //}
    }
}
