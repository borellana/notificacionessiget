﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using Microsoft.AspNetCore.Authorization;
using NotificacionesSIGET.Tools;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class UnidadesController : Controller
    {
        private readonly ApplicationDbContext _context;

        public UnidadesController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Unidades
        public async Task<IActionResult> Index()
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Index", "Se ingresa a Index de unidades", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                return View(await _context.unidad.ToListAsync());
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Unidades/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Details", "Se ingresa a Details del unidades", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var unidad = await _context.unidad
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (unidad == null)
                {
                    return NotFound();
                }

                return View(unidad);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Unidades/Create
        public IActionResult Create()
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Create", "Se ingresa a Create del unidades", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Unidades/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] Unidad unidad)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Create", "Se ingresa a Create del unidades", unidad.Id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (ModelState.IsValid)
            {
                _context.Add(unidad);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(unidad);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Unidades/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Edit", "Se ingresa a Edit del unidades", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
            {
                return NotFound();
            }

            var unidad = await _context.unidad.FindAsync(id);
            if (unidad == null)
            {
                return NotFound();
            }
            return View(unidad);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Unidades/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] Unidad unidad)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Edit", "Se ingresa a Edit del unidades", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id != unidad.Id)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(unidad);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!UnidadExists(unidad.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(unidad);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Unidades/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Delete", "Se ingresa a Delete del unidades", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var unidad = await _context.unidad
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (unidad == null)
                {
                    return NotFound();
                }

                return View(unidad);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Unidades/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-DeleteConfirmed", "Se elimina unidad", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                var unidad = await _context.unidad.FindAsync(id);
                _context.unidad.Remove(unidad);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        private bool UnidadExists(int id)
        {
            return _context.unidad.Any(e => e.Id == id);
        }
    }
}
