﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using NotificacionesSIGET.Areas.Identity.Data;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using NotificacionesSIGET.Tools;
using NotificacionesSIGET.ViewModels;

namespace NotificacionesSIGET.Controllers
{
    public class NotificacionesController : Controller
    {
        private readonly ApplicationDbContext _context;
        //private readonly IHostingEnvironment _hostingEnvironment;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly UserManager<AppUser> _userManager;
        private readonly IConfiguration _configuration;


        public NotificacionesController(ApplicationDbContext context,
          //                              IHostingEnvironment hostingEnvironment,
                                        RoleManager<IdentityRole> roleManager,
                                        UserManager<AppUser> userManager,
                                        IConfiguration configuration)
        {
            _context = context;
            //_hostingEnvironment = hostingEnvironment;
            _roleManager = roleManager;
            _userManager = userManager;
            _configuration = configuration;
        }

        [Authorize(Roles = "Notificador")]

        // GET: Notificaciones 
        public async Task<IActionResult> Index()
        {
            try
            {

                Herramientas.EscribirLog("Notificaciones-Index", "Se ingresa a Index del Notificaciones", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var applicationDbContext = _context.notificacion;

                var result = (from Notificacion in applicationDbContext
                              join u in _userManager.Users on Notificacion.PublicadoPor equals u.Id
                              select new Notificacion
                              {
                                  Id = Notificacion.Id,
                                  Comentario = Notificacion.Comentario,
                                  unidad = Notificacion.unidad,
                                  documento = Notificacion.documento,
                                  empresa = Notificacion.empresa,
                                  PublicadoPor = u.Nombres + " " + u.Apellidos,
                                  Fecha = Notificacion.Fecha,
                                  FechaDescarga = Notificacion.FechaDescarga,
                                  Notificado = Notificacion.Notificado
                              }).ToListAsync();

                return View(await result);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Notificaciones/Details/5 
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-Details", "Se ingresa a Details del Notificaciones", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var notificacion = await _context.notificacion
                    .Include(n => n.documento)
                    .Include(n => n.empresa)
                    .Include(n => n.unidad)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (notificacion == null)
                {
                    return NotFound();
                }
                return View(notificacion);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        [Authorize(Roles = "Notificador")]

        // GET: Notificaciones/Create 
        public IActionResult Create()
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-Create", "Se ingresa a Create del Notificaciones", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                ViewData["DocumentoId"] = new SelectList(_context.documentos, "id", "Nombre");
                ViewData["EmpresaId"] = new SelectList(_context.empresa, "Id", "Nombre");
                ViewData["UnidadId"] = new SelectList(_context.unidad, "Id", "Nombre");

                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }
        // GET: Notificaciones/Create 
        public IActionResult Creacion()
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-Create", "Se ingresa a Create del Notificaciones", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                ViewData["DocumentoId"] = new SelectList(_context.documentos, "id", "Nombre");
                ViewData["EmpresaId"] = new SelectList(_context.empresa, "Id", "Nombre");
                ViewData["UnidadId"] = new SelectList(_context.unidad, "Id", "Nombre");

                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }
        // POST: Notificaciones/Create 
        // To protect from overposting attacks, enable the specific properties you want to bind to, for  
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598. 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Notificador")]

        public async Task<IActionResult> Create([Bind("Id,Notificado,ComentarioNotificacion,UnidadId,DocumentoId,EmpresaId," +
            "Descripcion,Numero,Fecha,ListaEmpresas")] VMNotificado notificacion)
        {
            try
            {
                var usuarioId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
                Herramientas.EscribirLog("Notificaciones-Create", "Se Creo una Notificaciones", notificacion.Id.ToString(),
                                                            usuarioId, _context);
                if (ModelState.IsValid)
                {
                    notificacion.FechaDocumento = _context.documentos
                        .Where(x => x.id == notificacion.DocumentoId).Select(x => x.Fecha).FirstOrDefault();

                    notificacion.Fecha = DateTime.Now;

                    notificacion.DocumentoNumero = _context.documentos
                        .Where(x => x.id == notificacion.DocumentoId).Select(x => x.Numero).FirstOrDefault();

                    notificacion.TipoDocumentoNombre = _context.documentos.Include(n => n.tipoDocumento).
                        Where(x => x.id == notificacion.DocumentoId).Select(x => x.tipoDocumento.nombre).FirstOrDefault();

                    foreach (var empresa in notificacion.ListaEmpresas)
                    {
                        notificacion.EmpresaId = Convert.ToInt16(empresa);

                        Notificacion Notificacion = new Notificacion
                        {
                            Notificado = 1,
                            Comentario = notificacion.ComentarioNotificacion,
                            UnidadId = Convert.ToInt16(notificacion.UnidadId),
                            DocumentoId = Convert.ToInt16(notificacion.DocumentoId),
                            EmpresaId = Convert.ToInt16(empresa),
                            Fecha = DateTime.Now,
                            PublicadoPor = usuarioId
                        };

                        _context.Add(Notificacion);
                        _context.SaveChanges();

                        EnviaMail(notificacion);
                    }
                }

                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        private void EnviaMail(VMNotificado notificacion)
        {
            var usuarioId = User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var hora = Herramientas.HoraALetras(notificacion.Fecha);
            var fechaNotificacion = Herramientas.FechaALetras(notificacion.Fecha);

            notificacion.EmpresaNombre = _context.empresa.Where(x => x.Id == notificacion.EmpresaId)
                                         .Select(x => x.Nombre).FirstOrDefault();

            EnviaMail mail = new EnviaMail();

            var cuerpo = $"<table><tr><th><h2>Superintendencia General de Electricidad y Telecomunicaciones</h2>" +
                $"</th></tr><tr><th><h3>Sistema de Notificaciones Electrónicas SIGET</h3></th></tr><tr><th align='left'>" +
                $"{notificacion.EmpresaNombre} :<br /><br />Por este medio notifico que en la Superintendencia " +
                $"General de Electricidad y Telecomunicaciones se ha emitido el acto administrativo " +
                $"{notificacion.DocumentoNumero}. Para los efectos legales correspondientes, a " +
                $"continuación encontrará el enlace al Sistema de Notificaciones Electrónicas SIGET, desde " +
                $"el cual podrá acceder al acto acto administrativo." +
                $"<br /><br /><br />Notificador SNE.<br /><a href='http://notificaciones.siget.gob.sv'>" +
                $"Enlace de acceso: Sistema de Notificaciones SIGET</a></th></tr></table>";

            var usuarios = _userManager.Users.Where(x => x.empresa.Id == Convert.ToInt16(notificacion.EmpresaId)).ToList();

            foreach (var usuario in usuarios)
            {
                mail.EnviaCorreo(usuario.Email, cuerpo, _configuration);

                Herramientas.EscribirLog("Notificaciones-Create", "Se enviar correos a los miembros de la empresa: "
                    + usuario.UserName, notificacion.Id.ToString(), usuarioId, _context);
            }

            var listaNotificados = "";

            foreach (var PersonaANotificar in usuarios.Select(x=>x.Email))
            {
                listaNotificados += PersonaANotificar + ", ";
            }

            cuerpo = "";

            cuerpo = $"<table><tr><th><h2>Superintendencia General de Electricidad y Telecomunicaciones</h2>" +
                $"</th></tr><tr><th><h3>Sistema de Notificaciones Electrónicas SIGET</h3></th></tr><tr><th align='left'>Se " +
                $"hace constar que, el suscrito notificador SNE-SIGET notificó a las {hora} del {fechaNotificacion} " +
                $"(datos generados automáticamente por el Sistema), a {notificacion.EmpresaNombre} " +
                $"el acto administrativo N.° {notificacion.DocumentoNumero}. Para tal efecto, se remitió a " +
                $"la(s) dirección(es) de correo electrónico(s) {listaNotificados} proporcionada(s) por el administrado, " +
                $"un enlace para que pudiera acceder al texto del referido acto administrativo. Para los efectos legales " +
                $"correspondientes, se emite la presente acta de notificación, que se agregará al expediente " +
                $"administrativo del caso.<br /><br /><br /> Notificador SNE-SIGET <br />" +
                $"Notificador de la Superintendencia General de Electricidad y Telecomunicaciones <br /></th></tr></table>";

            var correo = _userManager.Users.Where(x => x.Id == usuarioId).Select(x => x.Email).FirstOrDefault();

            mail.EnviaCorreo(correo, cuerpo, _configuration);

            Herramientas.EscribirLog("Notificaciones-Create", "Se enviar correo al notificador: " + correo,
                 notificacion.Id.ToString(), usuarioId, _context);

            //Si es CARTA se envia correo al grupo, todas las cartas.
            if (notificacion.TipoDocumentoNombre.ToString() == "Carta")
            {
                var mailCartas = _context.parametros.Where(x => x.Nombre == "GrupoMailCarta").
                                Select(x => x.Valor).FirstOrDefault();

                mail.EnviaCorreo(mailCartas, cuerpo, _configuration);

                Herramientas.EscribirLog("Notificaciones-Create", "Se enviar correos a al grupo de cartas",
                    notificacion.Id.ToString(), usuarioId, _context);
            }
        }

        // GET: Notificaciones/Edit/5 
        [Authorize(Roles = "Notificador")]

        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-Edit", "Se ingresa a Edit del Notificaciones", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var notificacion = await _context.notificacion.FindAsync(id);
                if (notificacion == null)
                {
                    return NotFound();
                }
                ViewData["DocumentoId"] = new SelectList(_context.documentos, "id", "Nombre", notificacion.DocumentoId);
                ViewData["EmpresaId"] = new SelectList(_context.empresa, "Id", "Nombre", notificacion.EmpresaId);
                ViewData["UnidadId"] = new SelectList(_context.unidad, "Id", "Nombre", notificacion.UnidadId);
                return View(notificacion);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Notificaciones/Edit/5 
        // To protect from overposting attacks, enable the specific properties you want to bind to, for  
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598. 
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Notificador")]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Notificado,Comentario,UnidadId,DocumentoId,EmpresaId")] Notificacion notificacion)
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-Edit", "Se Edita Notificaciones", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id != notificacion.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(notificacion);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!NotificacionExists(notificacion.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                ViewData["DocumentoId"] = new SelectList(_context.documentos, "id", "Nombre", notificacion.DocumentoId);
                ViewData["EmpresaId"] = new SelectList(_context.empresa, "Id", "Nombre", notificacion.EmpresaId);
                ViewData["UnidadId"] = new SelectList(_context.unidad, "Id", "Nombre", notificacion.UnidadId);
                return View(notificacion);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Notificaciones/Delete/5 
        [Authorize(Roles = "Notificador")]

        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-Delete", "Se ingresa en Delete de Notificaciones", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var notificacion = await _context.notificacion
                    .Include(n => n.documento)
                    .Include(n => n.empresa)
                    .Include(n => n.unidad)
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (notificacion == null)
                {
                    return NotFound();
                }

                return View(notificacion);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Notificaciones/Delete/5 
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Notificador")]

        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-DeleteConfirmed", "Se ingresa en DeleteConfirmed de Notificaciones", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                var notificacion = await _context.notificacion.FindAsync(id);
                _context.notificacion.Remove(notificacion);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        [Authorize(Roles = "Notificador")]
        private bool NotificacionExists(int id)
        {
            return _context.notificacion.Any(e => e.Id == id);
        }

        [Authorize(Roles = "Usuario")]
        public async Task<IActionResult> Detalle()
        {
            try
            {
                var id = User.FindFirst(ClaimTypes.NameIdentifier).Value;

                //Herramientas.EscribirLog("Notificaciones-Detalle", "Se ingresa en Detalle de Notificaciones", "",
                //                         id, _context);

                var empresa = _userManager.Users.Where(x => x.Id == id).Select(x => x.empresa.Id).FirstOrDefault();

                var applicationDbContext = _context.notificacion
                    .Include(n => n.documento)
                    .Include(n => n.empresa)
                    .Include(n => n.unidad)
                    .Include(n => n.empresa.User)
                    .Where(n => n.empresa.Id == empresa)
                    .OrderByDescending(n => n.Notificado);

                //return View(applicationDbContext);
                return View(await applicationDbContext.ToListAsync());
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                throw ex;
                //return View();
            }

        }

        [Authorize(Roles = "Usuario")]
        public async Task<IActionResult> DescargaArchivo(int id)
        {
            try
            {
                Herramientas.EscribirLog("Notificaciones-DescargaArchivo", "Se descarga archivo en Notificaciones",
                                          id.ToString(), User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var notificacion = _context.notificacion.Find(id);

                var documento = _context.documentos.Find(notificacion.DocumentoId);

                // Marcamos los documentos de descargados. 
                var estaNotificado = _context.notificacion.Where(x => x.Notificado == 1).Count();

                if (estaNotificado != 0)
                {
                    notificacion.Notificado = 0;
                    notificacion.FechaDescarga = DateTime.Now;
                    _context.Update(notificacion);
                    await _context.SaveChangesAsync();
                }

                var ruta = Path.Combine(@"~\Documentos",
                            documento.Nombre);

                return File(ruta, System.Net.Mime.MediaTypeNames.Application.Octet, documento.Nombre);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        //[Route("/api/ReenviaMail")]
        public bool ReenviaMail(int id) {

            //var UserId = User.FindFirst(ClaimTypes.NameIdentifier).Value;

            var notificacion = _context.notificacion.Where(x => x.Id == id)
                                .Include(x=> x.documento)
                                .Include(x => x.documento.tipoDocumento)
                                .FirstOrDefault();

            VMNotificado vMNotificado = new VMNotificado
            {
                Id = notificacion.Id,
                Fecha = notificacion.Fecha,
                EmpresaId = notificacion.EmpresaId,
                DocumentoNumero = notificacion.documento.Numero,
                TipoDocumentoNombre = notificacion.documento.tipoDocumento.nombre
            };

            EnviaMail(vMNotificado);

            return true; 
        }
    }
}
