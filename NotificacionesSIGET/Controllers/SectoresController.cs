﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using Microsoft.AspNetCore.Authorization;
using NotificacionesSIGET.Tools;
using System.Security.Claims;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class SectoresController : Controller
    {
        private readonly ApplicationDbContext _context;

        public SectoresController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Sectores
        public async Task<IActionResult> Index()
        {
            try
            {

                Herramientas.EscribirLog("Sector-Index", "Se ingresa a Index del Sector", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                return View(await _context.sector.ToListAsync());
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Sectores/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {

                Herramientas.EscribirLog("Sector-Details", "Se ingresa a Details del Sector", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var sector = await _context.sector
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (sector == null)
                {
                    return NotFound();
                }

                return View(sector);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Sectores/Create
        public IActionResult Create()
        {
            try
            {
                Herramientas.EscribirLog("Sector-Create", "Se ingresa a Create del Sector", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Sectores/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre")] Sector sector)
        {
            try
            {
                Herramientas.EscribirLog("Sector-Create", "Se crea un Sector", sector.Id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (ModelState.IsValid)
                {
                    _context.Add(sector);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(sector);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Sectores/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Sector-Edit", "Se ingreso a Edit de Sector", id.ToString(),
                                          User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id == null)
                {
                    return NotFound();
                }

                var sector = await _context.sector.FindAsync(id);
                if (sector == null)
                {
                    return NotFound();
                }
                return View(sector);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Sectores/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre")] Sector sector)
        {
            try
            {
                Herramientas.EscribirLog("Sector-Edit", "Se edito Sector", id.ToString(),
                                          User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                if (id != sector.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(sector);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!SectorExists(sector.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(sector);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Sectores/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Sector-Delete", "Se ingreso a Delete de Sector", id.ToString(),
                                          User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var sector = await _context.sector
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (sector == null)
                {
                    return NotFound();
                }

                return View(sector);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Sectores/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {
                Herramientas.EscribirLog("Sector-DeleteConfirmed", "Se ingreso a DeleteConfirmed de Sector", id.ToString(),
                                          User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var sector = await _context.sector.FindAsync(id);
                _context.sector.Remove(sector);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        private bool SectorExists(int id)
        {
            return _context.sector.Any(e => e.Id == id);
        }
    }
}
