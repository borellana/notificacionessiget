﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using Microsoft.AspNetCore.Authorization;
using System.Security.Claims;
using NotificacionesSIGET.Tools;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Administrador")]
    public class ParametrosController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ParametrosController(ApplicationDbContext context)
        {
            _context = context;
        }

        // GET: Parametros
        public async Task<IActionResult> Index()
        {
            try
            {
                Herramientas.EscribirLog("Parametros-Index", "Se ingresa a Delete del Parametros", "",
                                               User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                return View(await _context.parametros.ToListAsync());
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Parametros/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Parametros-Details", "Se ingresa a Details del Parametros", id.ToString(),
                                               User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var parametros = await _context.parametros
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (parametros == null)
                {
                    return NotFound();
                }

                return View(parametros);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Parametros/Create
        public IActionResult Create()
        {
            try
            {
                Herramientas.EscribirLog("Parametros-Create", "Se ingresa a Create del Parametros", "",
                                               User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);
                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Parametros/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("Id,Nombre,Valor")] Parametros parametros)
        {
            try
            {
                Herramientas.EscribirLog("Parametros-Create", "se crea Parametros", parametros.Id.ToString(),
                                               User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (ModelState.IsValid)
                {
                    _context.Add(parametros);
                    await _context.SaveChangesAsync();
                    return RedirectToAction(nameof(Index));
                }
                return View(parametros);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Parametros/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            try
            {
                Herramientas.EscribirLog("Parametros-Edit", "Se ingreso a Edit de Parametros", id.ToString(),
                                               User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var parametros = await _context.parametros.FindAsync(id);
                if (parametros == null)
                {
                    return NotFound();
                }
                return View(parametros);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Parametros/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Nombre,Valor")] Parametros parametros)
        {
            try
            {

                Herramientas.EscribirLog("Parametro-Edit", "Se edita parametro", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id != parametros.Id)
                {
                    return NotFound();
                }

                if (ModelState.IsValid)
                {
                    try
                    {
                        _context.Update(parametros);
                        await _context.SaveChangesAsync();
                    }
                    catch (DbUpdateConcurrencyException)
                    {
                        if (!ParametrosExists(parametros.Id))
                        {
                            return NotFound();
                        }
                        else
                        {
                            throw;
                        }
                    }
                    return RedirectToAction(nameof(Index));
                }
                return View(parametros);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // GET: Parametros/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            try
            {

                Herramientas.EscribirLog("Parametro-Delete", "Se ingresa a Delete de parametro", id.ToString(),
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (id == null)
                {
                    return NotFound();
                }

                var parametros = await _context.parametros
                    .FirstOrDefaultAsync(m => m.Id == id);
                if (parametros == null)
                {
                    return NotFound();
                }

                return View(parametros);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        // POST: Parametros/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            try
            {

                Herramientas.EscribirLog("Parametro-DeleteConfirmed", "Se ingresa a DeleteConfirmed de parametro",
                                          id.ToString(), User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var parametros = await _context.parametros.FindAsync(id);
                _context.parametros.Remove(parametros);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        private bool ParametrosExists(int id)
        {
            return _context.parametros.Any(e => e.Id == id);
        }
    }
}
