﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.ViewModels;
using NotificacionesSIGET.Models;
using Microsoft.AspNetCore.Mvc.Rendering;
using NotificacionesSIGET.Areas.Identity.Data;
using Microsoft.AspNetCore.Authorization;
using NotificacionesSIGET.Tools;
using System.Security.Claims;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System.Security.Policy;
using System.Web;
using System.Text.Encodings.Web;

namespace NotificacionesSIGET.Controllers
{
    [Authorize(Roles = "Administrador, Notificador")]
    public class UsuariosController : Controller
    {

        private readonly UserManager<AppUser> _userManager;
        private readonly ApplicationDbContext _context;
        private readonly RoleManager<IdentityRole> _roleManager;
        private readonly IConfiguration _configuration;
        private readonly IPasswordHasher<AppUser> _passwordHasher;


        public UsuariosController(UserManager<AppUser> userManager,
                                  ApplicationDbContext context,
                                  RoleManager<IdentityRole> roleManager,
                                  IConfiguration configuration,
                                  IPasswordHasher<AppUser> passwordHash)
        {
            _userManager = userManager;
            _context = context;
            _roleManager = roleManager;
            _configuration = configuration;
            _passwordHasher = passwordHash;

        }

        public IActionResult Index()
        {
            try
            {
                Herramientas.EscribirLog("Usuarios-Index", "Se ingresa a Index de Usuarios", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                var usuarios = _userManager.Users.ToList();

                var VMUsuarios = (from usuario in usuarios
                                  let rol = _userManager.GetRolesAsync(usuario).Result.FirstOrDefault()
                                  select new VMUsuario
                                  {
                                      Id = usuario.Id,
                                      Nombres = usuario.Nombres,
                                      Apellidos = usuario.Apellidos,
                                      RolId = rol,
                                      Email = usuario.Email
                                  }).ToList();
                return View(VMUsuarios);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }

        }

        // GET: Usuarios/Details/5
        public async Task<IActionResult> Details(string id)
        {

            try
            {
                Herramientas.EscribirLog("Usuarios-Details", "Ingreso a Details de usuario", id.ToString(),
                                         User.FindFirst(ClaimTypes.NameIdentifier).Value, _context); ;

                if (id == null)
                {
                    return NotFound();
                }

                AppUser usuario = await _userManager.FindByIdAsync(id);

                if (usuario == null)
                {
                    return NotFound();
                }

                return View(usuario);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        public ActionResult Create()
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Create", "Se ingresa a Create de unidades", "",
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                ViewData["EmpresaId"] = new SelectList(_context.empresa.ToList(), nameof(Empresa.Id), nameof(Empresa.Nombre));
                ViewData["RolId"] = new SelectList(_context.Roles, "Id", "Name");
                ViewData["TipoPersonaId"] = new SelectList(_context.TipoPersonas, "Id", "Nombre");
                return View();
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create(VMUsuario VMUsuarios)
        {
            try
            {
                Herramientas.EscribirLog("Unidades-Create", "Se crea unidades", VMUsuarios.Id,
                                                            User.FindFirst(ClaimTypes.NameIdentifier).Value, _context);

                if (ModelState.IsValid)
                {
                    var user = new AppUser
                    {
                        Email = VMUsuarios.Email,
                        Nombres = VMUsuarios.Nombres,
                        Apellidos = VMUsuarios.Apellidos,
                        UserName = VMUsuarios.Email,
                        empresa = _context.empresa.Find(VMUsuarios.EmpresaId),
                        TipoPersonaId = VMUsuarios.TipoPersonaId,
                        EmailConfirmed = true, 
                        FirstPassword = true
                    };

                    VMUsuarios.Password = generaClave();
                    var result = await _userManager.CreateAsync(user, VMUsuarios.Password);
                    if (result.Succeeded)
                    {

                        var nombreRol = _roleManager.FindByIdAsync(VMUsuarios.RolId).Result.Name;
                        await _userManager.AddToRoleAsync(user, nombreRol);
                        await _context.SaveChangesAsync();


                        ////---------------------------

                        //var code = await _userManager.GenerateEmailConfirmationTokenAsync(user);

                        //var callbackUrl = Url.Action("ConfirmEmail", "Account", new {userid = user.Id, code = code },
                        //    protocol: Request.Scheme, Request.Host.Value + "/Identity");

                        //var linkVerificacion = HtmlEncoder.Default.Encode(callbackUrl);

                        //var cuerpo = $"Por favor confirmar <a href ='{linkVerificacion}'>link</a>";

                        ////---------------------------


                        //enviar correo.
                        //ToDo: Cambiar este string a la tabla de parametros 
                        var cuerpo = $"<html><body><table><tr><th><h3>Superintendencia General de Electricidad y Telecomunicaciones</h3>" +
                                     $"</th></tr><tr><th><h2>Sistema de Notificaciones SIGET</h2></th></tr><tr><th align='left'> " +
                                     $"Bienvenido/a {VMUsuarios.Apellidos},{VMUsuarios.Nombres}, al Sistema de Notificaciones de la " +
                                     $"Superintendencia General de Electricidad y Telecomunicaciones, para poder hacer uso de este creo una cuenta, " +
                                     $"esta es la informacion:<br /><br />Usuario: {VMUsuarios.Email}<br />Clave : {VMUsuarios.Password}<br />" +
                                     $"<br />Puede ingresar al sistema por medio de esta direccion: <a href='http://notificaciones.siget.gob.sv'>" +
                                     $"http://notificaciones.siget.gob.sv</a><br /></th></tr></table></body></html>";

                        _context.parametros.Where(x => x.Nombre == "CorreoBienvenida")
                                 .Select(x => x.Valor).FirstOrDefault();

                        EnviaMail mail = new EnviaMail();
                        mail.EnviaCorreo(VMUsuarios.Email, cuerpo, _configuration);

                        return RedirectToAction(nameof(Index));
                    }
                    foreach (var error in result.Errors)
                    {
                        ModelState.AddModelError(string.Empty, error.Description);
                    }
                }

                ViewData["EmpresaId"] = new SelectList(_context.empresa.ToList(), nameof(Empresa.Id), nameof(Empresa.Nombre));
                ViewData["RolId"] = new SelectList(_context.Roles, "Id", "Name");
                return View(VMUsuarios);
            }
            catch (Exception ex)
            {
                Herramientas.EscribirLog(ex, _context);
                return View();
            }
        }

        private string generaClave()
        {
            Random rdn = new Random();
            string caracteres = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            int longitud = caracteres.Length;
            char letra;
            int longitudContrasenia = 8;
            string claveAleatoria = string.Empty;
            for (int i = 0; i < longitudContrasenia; i++)
            {
                letra = caracteres[rdn.Next(longitud)];
                claveAleatoria += letra.ToString();
            }
            caracteres = "%$#@";
            longitud = caracteres.Length;
            letra = caracteres[rdn.Next(longitud)];
            claveAleatoria += letra.ToString();

            caracteres = "1234567890";
            longitud = caracteres.Length;
            letra = caracteres[rdn.Next(longitud)];
            claveAleatoria += letra.ToString();

            return claveAleatoria;
        }

        // POST: Usuarios/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit(String id, [Bind("Nombres,Apellidos,RolId,EmpresaId,Email,TipoPersonaId")] VMUsuario VMUsuarios)
        {
            if (ModelState.IsValid)
            {
                var usuario = _userManager.FindByIdAsync(id).Result;
                var nombreRol = _userManager.GetRolesAsync(usuario).Result.FirstOrDefault();
                var rolUsuario = _roleManager.FindByNameAsync(nombreRol).Result;
                var rolUsuarioNuevo = _roleManager.Roles.FirstOrDefault(x => x.Id == VMUsuarios.RolId);

                usuario.Nombres = VMUsuarios.Nombres;
                usuario.Apellidos = VMUsuarios.Apellidos;
                usuario.Email = VMUsuarios.Email;
                usuario.TipoPersonaId = VMUsuarios.TipoPersonaId;
                usuario.empresa = _context.empresa.Find(VMUsuarios.EmpresaId);
                await _context.SaveChangesAsync();

                if (rolUsuario.Name != rolUsuarioNuevo.Name)
                {
                    await _userManager.RemoveFromRoleAsync(usuario, rolUsuario.Name);
                    await _userManager.AddToRoleAsync(usuario, rolUsuarioNuevo.Name);
                }
                return RedirectToAction(nameof(Index));
            }
            return View(VMUsuarios);
        }

        //GET
        public async Task<IActionResult> Edit(string id)
        {

            if (id == null)
            {
                return NotFound();
            }

            var usuarios = _userManager.FindByIdAsync(id).Result;
            var nombreRol = _userManager.GetRolesAsync(usuarios).Result.FirstOrDefault();
            var rol = _roleManager.FindByNameAsync(nombreRol).Result;

            var VMUsuarios = new VMUsuario
            {
                Nombres = usuarios.Nombres,
                Apellidos = usuarios.Apellidos,
                RolId = rol.Id,
                Email = usuarios.Email,
                TipoPersonaId = usuarios.TipoPersonaId
            };

            if (VMUsuarios == null)
            {
                return NotFound();
            }

            ViewData["EmpresaId"] = new SelectList(_context.empresa.ToList(), "Id", "Nombre", usuarios.empresa.Id);
            ViewData["RolId"] = new SelectList(_context.Roles, "Id", "Name",VMUsuarios.RolId);
            ViewData["TipoPersonaId"] = new SelectList(_context.TipoPersonas, "Id", "Nombre", VMUsuarios.TipoPersonaId);
            return View(VMUsuarios);
        }

        // POST: Usuarios/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(string id)
        { 
            var VMusuarios = await _context.Users.FindAsync(id);
            _context.Users.Remove(VMusuarios);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool VMUsuarioExists(string id)
        {
            return _context.Users.Any(e => e.Id == id);
        }

        [AllowAnonymous]
        public async Task<ActionResult> CambioClave() {
            AppUser usuario = await _userManager.GetUserAsync(User);
            return View(usuario);
        }

        [AllowAnonymous]
        [HttpPost]
        public async Task<ActionResult> CambioClave(string Id, string pwd1, string pwd2)
        {
            AppUser user = await _userManager.FindByIdAsync(Id);

            if (string.IsNullOrEmpty(pwd1))
            {
                ModelState.AddModelError("", "La contraseña no puede ser vacio");
                return View(user);
            }

            if (pwd1 != pwd2)
            {
                ModelState.AddModelError("", "Las claves debe de ser iguales.");
                return View(user);
            }

            if (user == null)
            {
                ModelState.AddModelError("", "Usuario no encontrado.");
                return View(user);
            }

            user.FirstPassword = false;
            user.PasswordHash = _passwordHasher.HashPassword(user, pwd1);
            IdentityResult result = await _userManager.UpdateAsync(user);
            if (result.Succeeded)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                foreach (IdentityError error in result.Errors)
                    ModelState.AddModelError("", error.Description);
            }

            return View(user);
        }

        public async Task<ActionResult> ResetPassword(string Id) {

            var clave = generaClave();

            AppUser usuario = await _userManager.FindByIdAsync(Id);

            usuario.PasswordHash = _passwordHasher.HashPassword(usuario, clave);

            //var result = await _userManager.ChangePasswordAsync(usuario, usuario.PasswordHash, 
            //    _passwordHasher.HashPassword(usuario, clave));

            IdentityResult result = await _userManager.UpdateAsync(usuario);

            if (result.Succeeded)
            {
                var cuerpo = $"<table><tr><th><h2>Superintendencia General de Electricidad y Telecomunicaciones</h2>" +
                     $"</th></tr><tr><th><h3>Sistema de Notificaciones SIGET</h3></th></tr><tr><th align='left'> " +
                     $"Hemos realizado el cambio de su clave del Sistema de Notificaciones de la " +
                     $"Superintendencia General de Electricidad y Telecomunicaciones, " +
                     $"esta es la informacion:<br /><br />Usuario: {usuario.Email}<br />Clave : {clave}<br />" +
                     $"<br />Puede ingresar al sistema por medio de esta direccion: <a href='http://notificaciones.siget.gob.sv'>" +
                     $"http://notificaciones.siget.gob.sv</a><br /></th></tr></table>";

                EnviaMail mail = new EnviaMail();

                mail.EnviaCorreo(usuario.Email, cuerpo, _configuration);

                return RedirectToAction(nameof(Index));
            }

            return View();
        }

    }
}