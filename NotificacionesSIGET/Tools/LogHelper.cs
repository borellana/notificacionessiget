﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using Microsoft.AspNetCore.Identity;

namespace NotificacionesSIGET.Tools
{
    public class LogHelper
    {
        public static void writeException(Exception e, ApplicationDbContext context)
        {
            LogNotificaciones notificaciones = new LogNotificaciones();
            notificaciones.Fecha = DateTime.Now;
            notificaciones.Tipo = "Error";
            notificaciones.Comentario = "MESSAGE: " + e.Message + " - STACKTRACE: " + e.StackTrace;

            if (e.InnerException != null)
            {
                notificaciones.Comentario += "INNEREXCEPTION.MESSAGE: " + e.InnerException.Message 
                    + " - INNEREXCEPTION.STACKTRACE: " + e.InnerException.StackTrace;
            }

            context.log.Add(notificaciones);
            context.SaveChanges();
        }

        public static void writeLog(LogNotificaciones notificaciones, ApplicationDbContext context)
        {
            notificaciones.Tipo = "Info";
            context.log.Add(notificaciones);
            context.SaveChanges();
        }
    }
}
