﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Tools
{
    public class EnviaMail
    {
        public bool EnviaCorreo(string destino, string cuerpo, IConfiguration _configuration)
        {
            MailMessage msg = new MailMessage();
 //destino = "orellana.balmore@gmail.com";
            msg.To.Add(new MailAddress(destino));
            msg.From = new MailAddress("notificacion@siget.gob.sv", "Notificaciones SIGET");
            msg.Subject = "Sistema de Notificaciones de SIGET";
            msg.Body = cuerpo;
            msg.IsBodyHtml = true;
            //SmtpClient client = new SmtpClient();
            SmtpClient client = new SmtpClient(_configuration.GetValue<string>("mail"),
                                               _configuration.GetValue<int>("mailPort"));
            client.UseDefaultCredentials = false;
            client.Credentials = new System.Net.NetworkCredential(_configuration.GetValue<string>("mail"),
                                                                  _configuration.GetValue<string>("mailPassword"));
            client.Port = _configuration.GetValue<int>("mailPort"); // You can use Port 25 if 587 is blocked (mine is!)

            client.Host = _configuration.GetValue<string>("mailHost");
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;

            client.Send(msg);
            return true;
        }
    }
}
