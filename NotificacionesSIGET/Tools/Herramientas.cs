﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using NotificacionesSIGET.Areas.Identity.Data;
using NotificacionesSIGET.Data;
using NotificacionesSIGET.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;


namespace NotificacionesSIGET.Tools
{
    public class Herramientas
    {
        private static LogNotificaciones LlenaLog(string accion, string comentario, string documento, string usuario)
        {

            LogNotificaciones log = new LogNotificaciones();

            log.Accion = accion;
            log.Comentario = comentario;
            log.Documento = documento;
            log.Fecha = DateTime.Now;
            log.Usuario = usuario;

            return log;
        }

        public static void EscribirLog(Exception ex, ApplicationDbContext context)
        {
            Tools.LogHelper.writeException(ex, context);
        }

        public static void EscribirLog(string accion, string comentario, string documento, string usuario, ApplicationDbContext context)
        {
            LogHelper.writeLog(LlenaLog(accion, comentario, documento, usuario), context);
        }

        public static string FechaALetras(DateTime fecha)
        {
            string result = " ";

            result += NumeroALetras(fecha.Day);

            DateTimeFormatInfo formatoFecha = CultureInfo.CurrentCulture.DateTimeFormat;
            result += " de " + formatoFecha.GetMonthName(fecha.Month);

            result += " de " + NumeroALetras(fecha.Year);

            return result;
        }

        public static string HoraALetras(DateTime fecha)
        {

            return NumeroALetras(fecha.Hour) + " horas con " + NumeroALetras(fecha.Minute) + " minutos";

        }

        private static string NumeroALetras(double value)
        {
            string num2Text = "";
            value = Math.Truncate(value);
            if (value == 0) num2Text = "cero";
            else if (value == 1) num2Text = "uno";
            else if (value == 2) num2Text = "dos";
            else if (value == 3) num2Text = "tres";
            else if (value == 4) num2Text = "cuatro";
            else if (value == 5) num2Text = "cinco";
            else if (value == 6) num2Text = "seis";
            else if (value == 7) num2Text = "siete";
            else if (value == 8) num2Text = "ocho";
            else if (value == 9) num2Text = "nueve";
            else if (value == 10) num2Text = "diez";
            else if (value == 11) num2Text = "once";
            else if (value == 12) num2Text = "doce";
            else if (value == 13) num2Text = "trece";
            else if (value == 14) num2Text = "catorce";
            else if (value == 15) num2Text = "quince";
            else if (value < 20) num2Text = "dieci" + NumeroALetras(value - 10);
            else if (value == 20) num2Text = "veinte";
            else if (value < 30) num2Text = "veinti" + NumeroALetras(value - 20);
            else if (value == 30) num2Text = "treinta";
            else if (value == 40) num2Text = "cuarenta";
            else if (value == 50) num2Text = "cincuenta";
            else if (value == 60) num2Text = "sesenta";
            else if (value == 70) num2Text = "setenta";
            else if (value == 80) num2Text = "ochenta";
            else if (value == 90) num2Text = "noventa";
            else if (value < 100) num2Text = NumeroALetras(Math.Truncate(value / 10) * 10) + " y " + NumeroALetras(value % 10);
            else if (value == 100) num2Text = "cien";
            else if (value < 200) num2Text = "ciento " + NumeroALetras(value - 100);
            else if ((value == 200) || (value == 300) || (value == 400) || (value == 600) || (value == 800)) num2Text = NumeroALetras(Math.Truncate(value / 100)) + "cientos";
            else if (value == 500) num2Text = "quinientos";
            else if (value == 700) num2Text = "setecientos";
            else if (value == 900) num2Text = "novecientos";
            else if (value < 1000) num2Text = NumeroALetras(Math.Truncate(value / 100) * 100) + " " + NumeroALetras(value % 100);
            else if (value == 1000) num2Text = "mil";
            else if (value < 2000) num2Text = "mil " + NumeroALetras(value % 1000);
            else if (value < 1000000)
            {
                num2Text = NumeroALetras(Math.Truncate(value / 1000)) + " mil";
                if ((value % 1000) > 0)
                {
                    num2Text = num2Text + " " + NumeroALetras(value % 1000);
                }
            }

            return num2Text;
        }
    }
}
