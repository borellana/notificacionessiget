﻿using System;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using NotificacionesSIGET.Models;
using System.ComponentModel.DataAnnotations;
using System.Security.Policy;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace NotificacionesSIGET.ViewModels
{
    public class VMNotificado
    {
        public VMNotificado() { }

        public VMNotificado(Notificacion notificacion, Documento documento, Empresa empresa, Unidad unidad)
        {
            Id = notificacion.Id;
            Notificado = notificacion.Notificado;
            EmpresaId = empresa.Id;
            EmpresaNombre = empresa.Nombre;
            RepresentanteEmpresa = empresa.Representante;
            Notificacion = notificacion.Comentario;
            NombreArchivo = documento.Nombre;
            Descripcion = documento.descripcion; //archivo
            FechaDocumento = documento.Fecha;
            DocumentoNumero = documento.Numero;
            UnidadNombre = unidad.Nombre;
            UnidadId = unidad.Id;
            DocumentoId = notificacion.DocumentoId;
            ComentarioNotificacion = notificacion.Comentario;
            TipoDocumentoNombre = documento.tipoDocumento.nombre;
        }

        public int? Id { get; set; }
        public int? Notificado { get; set; }
        public string Notificacion { get; set; }
        public string NombreArchivo { get; set; }
        public string Descripcion { get; set; }
        public int? DocumentoId { get; set; }
        [Display(Name = "Empresa")]
        public int? EmpresaId { get; set; }
        [Display(Name = "Nombre Empresa")]
        public string EmpresaNombre { get; set; }
        [Display(Name = "Unidad")]
        public int? UnidadId { get; set; }
        public string UnidadNombre { get; set; }
        public IFormFile FFArchivo { get; set; }
        public string descripcion { get; set; }
        public string Numero { set; get; }
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        public DateTime Fecha { set; get; }
        public DateTime FechaDocumento { set; get; }
        public string RepresentanteEmpresa { get; set; }
        public string DocumentoNumero { get; set; }
        public string TipoDocumentoNombre { get; set; }
        [Display(Name = "Comentario")]
        public string ComentarioNotificacion { get; set; }


        public string[] ListaEmpresas { get; set; }

        //public IFormFile Archivo { get; set; }
        //public string ArchivoURL { get; set; }
    }
}
