﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.ViewModels
{
    public class VMRol
    {
        public VMRol() { }

        public VMRol(IdentityRole rol)
        {
            Id = rol.Id;
            RolName = rol.NormalizedName;
        }

        [Required]
        [Display(Name = "Rol")]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Nombre de Rol")]
        public string RolName { get; set; }
    }
}
