﻿using Microsoft.AspNetCore.Identity;
using NotificacionesSIGET.Areas.Identity.Data;
using NotificacionesSIGET.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.ViewModels
{
    public class VMUsuario
    {
        public VMUsuario() { }
        public VMUsuario(AppUser usuario, IdentityRole rol, Empresa datosEmpresa)
        {
            Id = usuario.Id;
            UserName = usuario.Email;
            Nombres = usuario.Nombres;
            Apellidos = usuario.Apellidos;
            Email = usuario.Email;
            RolId = rol.Id;
            EmpresaId = datosEmpresa.Id;
            EmpresaNombre = datosEmpresa.Nombre;
            TipoPersona = usuario.TipoPersona.nombre;
            TipoPersonaId = usuario.TipoPersonaId;
        }

        public string Id { get; set; }
        public string UserName { get; set; }
        [Required]
        public string Nombres { get; set; }
        [Required]
        public string Apellidos { get; set; }
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Rol")]
        public string RolId { get; set; }
        [Display(Name = "Empresa")]
        public int? EmpresaId { get; set; }
        public string EmpresaNombre { get; set; }
        public string Password { get; set; }
        [Display(Name = "Tipo Persona")]
        public string TipoPersona { get; set; }
        [Display(Name = "Tipo Persona")]
        public int TipoPersonaId { get; set; }




    }
}
