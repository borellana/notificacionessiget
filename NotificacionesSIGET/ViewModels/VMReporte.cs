﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using NotificacionesSIGET.Models;

namespace NotificacionesSIGET.ViewModels
{
    public class VMReporte
    {
        public VMReporte() { }

        public VMReporte(Notificacion notificacion, Documento documento, Unidad unidad, Empresa empresa) {
            Id = notificacion.Id;
            Comentario = notificacion.Comentario;
            FechaCarga = notificacion.Fecha;
            fechaDescarga = notificacion.FechaDescarga;
            NombreDocumento = documento.Nombre;
            DescripcionDocumento = documento.descripcion;
            NumeroDocumento = documento.Numero;
            UnidadNombre = unidad.Nombre;
            EmpresaNombre = empresa.Nombre;
        }

        public int Id;
        public string Comentario;
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        [Display(Name = "Fecha Notificacion")]
        public DateTime FechaCarga;
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{dd/MM/yyyy}")]
        [Display(Name = "Fecha Descarga")]
        public DateTime fechaDescarga;
        [Display(Name = "Nombre documento")]
        public string NombreDocumento;
        [Display(Name = "Nombre Descripcion")]
        public string DescripcionDocumento;
        [Display(Name = "Numero Documento")]
        public string NumeroDocumento;
        [Display(Name = "Unidad Nombre")]
        public string UnidadNombre;
        [Display(Name = "Empresa Nombre")]
        public string EmpresaNombre;
    }
}
