﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class TipoDocumento
    {
        public int id { get; set; }
        [Display(Name = "Tipo de Documento")]
        public string nombre { get; set; }
    }
}
