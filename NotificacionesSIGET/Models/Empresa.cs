﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using NotificacionesSIGET.Areas.Identity.Data;

namespace NotificacionesSIGET.Models
{
    public class Empresa
    {
        public int Id { get; set; }
        [Display(Name = "Nombre Empresa")]
        public string Nombre { get; set; }
        [Display(Name = "Representante")]
        public string Representante { get; set; }
        [Display(Name = "Sector")]
        public Sector sector { get; set; }
        [Display(Name = "Sector")]
        public int SectorId { get; set; }

        public virtual ICollection<AppUser> User { get; set; }

    }
}
