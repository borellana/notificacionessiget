﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class LogNotificaciones
    {
        public int Id { get; set; }
        public DateTime Fecha { set; get; }
        public string Tipo { get; set; }
        public string Usuario { get; set; }
        public string Accion { get; set; }
        public string ip { get; set; }
        public string Documento { get; set; }
        public string Comentario { get; set; }
         
    }
}
