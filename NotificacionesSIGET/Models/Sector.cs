﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class Sector
    {
        public int Id { set; get; }
        [Display(Name = "Nombre Sector")]
        public string Nombre { set; get; }

 
    }
}
