﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class Documento
    {
        public int id { get; set; }
        [Display(Name = "Nombre Documento")]
        public string Nombre { set; get; }
        [NotMapped]
        public IFormFile FFArchivo { get; set; }
        [Display(Name = "Descripcion")]
        public string descripcion { get; set; }
        [Display(Name = "Numero de Documento")]
        public string Numero { set; get; }

        public DateTime Fecha {set;get;}
        [Display(Name = "Tipo de Documento")]
        public TipoDocumento tipoDocumento { get; set; }
        [Display(Name = "Tipo de Documento")]
        public int TipoDocumentoId { get; set; }
        [Display(Name ="Publicado por")]
        public string PublicadoPor { get; set; }



    }
}
