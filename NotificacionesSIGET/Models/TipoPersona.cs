﻿using NotificacionesSIGET.Areas.Identity.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class TipoPersona
    {
        internal string nombre;

        public int Id { get; set; }
        [Display(Name = "Tipo Persona")]
        public string Nombre { get; set; }

        public virtual ICollection<AppUser> User { get; set; }

    }
}
