﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class Parametros
    {
        public int Id { get; set; }
        [Display(Name = "Nombre Parametro")]
        public string Nombre { get; set; }
        [Display(Name = "Valor de Parametro")]
        public string Valor { get; set; }
    }
}
