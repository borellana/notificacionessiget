﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class Notificacion
    {
        public int Id { get; set; }
        //Señal de que fue descargado en documento.
        public int Notificado { get; set; }
        public string Comentario { get; set; }
        [Display(Name = "Fecha notificacion")]
        public DateTime Fecha { get; set; }
        [Display(Name = "Fecha descarga")]
        public DateTime FechaDescarga { get; set; }
        [Display(Name = "Publicado por")]
        public string PublicadoPor { get; set; }

        //private DateTime Fecha { set => Fecha = DateTime.Now; }



        // Relaciones.
        [Display(Name = "Unidad")]
        public Unidad unidad { get; set; }
        [Display(Name = "Unidad")]
        public int UnidadId { get; set; }
        [Display(Name = "Documento")]
        public Documento documento { get; set; }
        [Display(Name = "Documento")]
        public int DocumentoId { get; set; }
        [Display(Name = "Empresa")]
        public Empresa empresa { get; set; }
        [Display(Name = "Empresa")]
        public int EmpresaId { get; set; }
    }
}