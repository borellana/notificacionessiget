﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class GrupoEmpresa
    {
        public Empresa Empresa { get; set; }
        public int EmpresaId { get; set; }
        //public int empresa { get; set; }
        [Display(Name = "Nombre grupo")]
        public Grupo Grupo { get; set; }
        public int GrupoId { get; set; }

        //public int grupo { get; set; }
        [NotMapped]
        public int Cuenta { get; set; }
        [NotMapped]
        public string Nombre { get; set; }
    }
}
