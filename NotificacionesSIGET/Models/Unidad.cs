﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace NotificacionesSIGET.Models
{
    public class Unidad
    {
        public int Id { get; set;}
        [Display(Name = "Nombre Unidad")]
        public string Nombre { get; set; }
    }
}
